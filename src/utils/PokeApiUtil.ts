import { POKEMON_API_URL, PokemonTypes } from '../constants';

export async function getPokemon(name: string | number = 'pikachu') {
    if (!name) {
        return null;
    }
    const response = await fetch(`${POKEMON_API_URL}${name}/`);
    if (!response.ok) {
        return null;
    }
    const data = await response.json().then((data) => {
        return data;
    });

    const types = data.types.map((item: PokemonTypes ) => {
        const container = [];
        container.push(item.type.name);
        return container;
    });

    return {
        id: data.id,
        name: data.name,
        order: data.order,
        weight: data.weight,
        sprites: data.sprites.front_default,
        height: data.height,
        types
    };
}
