import React from 'react';
import { render } from '@testing-library/react';
import App from '../App';

describe('Test <App />', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.resetAllMocks();
  });

  it('should display search bar', () => {
    const { getByText } = render(<App />);
    expect(getByText('Search')).toBeInTheDocument();
    expect(getByText('Result:')).toBeInTheDocument();
    expect(getByText('Previously Searched:')).toBeInTheDocument();
  })
})