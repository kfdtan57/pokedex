import React from 'react';
import { POKEMON_MODEL } from '../constants';
import { capitalizeFirstLetter } from '../utils/StringUtil';

interface PokemonDisplayComponentProps {
    pokemon: POKEMON_MODEL;
}

export const PokemonDisplayComponent: React.FunctionComponent<PokemonDisplayComponentProps> = ({
    pokemon,
    children
}) => {

    const getAltText = (imgUrl: string) => {
        if (imgUrl) {
            return `Default front image of ${pokemon.name}`;
        } else {
            return '';
        }
    };

    return (
        <div key={pokemon.id}>
            <img alt={getAltText(pokemon.sprites)} src={pokemon.sprites} />
            <div>#{pokemon.id}</div>
            <div>{capitalizeFirstLetter(pokemon.name)}</div>
            {children}
        </div>
    );
};
