import React from 'react';
import { POKEMON_MODEL } from '../constants';
import { PokemonDisplayComponent } from '../PokemonDisplayComponent/PokemonDisplayComponent';

interface DisplayMultiplePokemonComponentProps {
    currentPokemon: POKEMON_MODEL;
    otherPokemon: POKEMON_MODEL[];
}

export const DisplayMultiplePokemonComponent: React.FunctionComponent<DisplayMultiplePokemonComponentProps> =
    ({ currentPokemon, otherPokemon }) => {
        return (
            <>
                {otherPokemon.map((pokemon) => (
                    <PokemonDisplayComponent key={pokemon.id} pokemon={pokemon}>
                        <div>Weight: {pokemon.weight}</div>
                        <div>Height: {pokemon.height}</div>
                        <div>Types: {pokemon.types.join(', ')}</div>
                    </PokemonDisplayComponent>
                ))}
            </>
        );
    };
