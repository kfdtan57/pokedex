import React, { useEffect, useRef, useState } from 'react';
import './App.css';
import { getPokemon } from './utils/PokeApiUtil';
import { EMPTY_POKEMON_STATS, POKEMON_MODEL } from './constants';
import { SearchBarComponent } from './SearchBarComponent/SearchBarComponent';
import { PokemonDisplayComponent } from './PokemonDisplayComponent/PokemonDisplayComponent';
import { DisplayMultiplePokemonComponent } from './DisplayMultiplePokemonComponent/DisplayMultiplePokemonComponent';
import { DisplaySearchedPokemonComponent } from './DisplayMultiplePokemonComponent/DisplaySearchedPokemonComponent';

function App() {
    const [pokemon, setPokemon] = useState<POKEMON_MODEL>(EMPTY_POKEMON_STATS);
    const [error, setError] = useState(false);
    const [searchedPokemon, setSearchedPokemon] = useState<POKEMON_MODEL[]>([]);
    const [searchedPokemonIds, setSearchedPokemonIds] = useState<string[]>([]);
    const caughtPokemonRef = useRef<POKEMON_MODEL[]>([]);
    const [caughtPokemon, setCaughtPokemon] = useState<POKEMON_MODEL[]>([]);

    // Load caught pokemon
    useEffect(() => {
        const store = localStorage.getItem('caughtPokemon');
        if (store !== null) {
            const caughtPokemonIds = store.split(',');
            let catchingPokemon: POKEMON_MODEL[] = [];
            for (let id of caughtPokemonIds) {
                getPokemon(id).then((value) => {
                    if (value != null) {
                        catchingPokemon.push(value);
                    }
                });
            }
            caughtPokemonRef.current = catchingPokemon;
        }
    });

    // Update page only when pokemon state updates
    useEffect(() => {
        if (pokemon.id) {
            searchedPokemon.push(pokemon);
            searchedPokemonIds.push(pokemon.id.toString());

            const uniqueArray = searchedPokemon.filter(
                (searchPokemon, index, self) =>
                    index === self.findIndex((t) => t.id === searchPokemon.id)
            );
            setSearchedPokemon(uniqueArray);
            setSearchedPokemonIds([...new Set(searchedPokemonIds)]);

            console.log('Saved! Searched pokemon length: ', searchedPokemon.length);
        }
        // eslint-disable-next-line
    }, [pokemon]);

    const searchPokemon = (search: any) => {
        getPokemon(search).then((value) => {
            if (value != null) {
                setPokemon(value);
                setError(false);
            } else {
                setPokemon(EMPTY_POKEMON_STATS);
                setError(true);
            }
        });
    };

    // eslint-disable-next-line
    const handleCatch = (e: React.SyntheticEvent) => {
        e.preventDefault();
        catchPokemon();
    };

    const catchPokemon = () => {
        caughtPokemonRef.current.push(pokemon);
        caughtPokemonRef.current = caughtPokemonRef.current.filter(
            (save, index, self) => index === self.findIndex((t) => t.id === save.id)
        );

        localStorage.setItem(
            'caughtPokemon',
            caughtPokemonRef.current.map((caught) => caught.id.toString()).join(',')
        );
    };

    return (
        <div className="App">
            <header className="App-header">
                <h1>Pokédex</h1>
                <SearchBarComponent performSearch={searchPokemon} />
                <h3>Result:</h3>
                {pokemon.name && (
                    <button type="button" onClick={handleCatch}>
                        Caught it!
                    </button>
                )}
                {pokemon.name && (
                    <PokemonDisplayComponent pokemon={pokemon}>
                        <div>Weight: {pokemon.weight}</div>
                        <div>Height: {pokemon.height}</div>
                        <div>Types: {pokemon.types.join(', ')}</div>
                    </PokemonDisplayComponent>
                )}
                {error && <>Error finding pokemon. Please try a different search term!</>}
                <h3>Previously Searched: </h3>
                <DisplaySearchedPokemonComponent
                    currentPokemon={pokemon}
                    otherPokemon={searchedPokemon}
                />
                <h3>Caught {caughtPokemon.length} Pokémon: </h3>
                <button onClick={() => setCaughtPokemon(caughtPokemonRef.current)}>
                    Load caught Pokémon
                </button>
                <DisplayMultiplePokemonComponent
                    currentPokemon={pokemon}
                    otherPokemon={caughtPokemon}
                />
            </header>
        </div>
    );
}

export default App;
