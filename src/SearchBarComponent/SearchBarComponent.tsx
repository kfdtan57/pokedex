import React, { useState } from 'react';

interface SearchBarComponentProps {
    performSearch: (searchTerm: string) => void;
}

export const SearchBarComponent: React.FunctionComponent<SearchBarComponentProps> = ({
    performSearch
}) => {
    const [search, setSearch] = useState('');

    // eslint-disable-next-line
    const handleSubmit = (e: React.SyntheticEvent) => {
        e.preventDefault();
        performSearch(search);
    };

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <input type="text" value={search} onChange={(e) => setSearch(e.target.value)} />
            <div>
                <input type="submit" value="Search Pokédex!" name="search" />
            </div>
        </form>
    );
};
