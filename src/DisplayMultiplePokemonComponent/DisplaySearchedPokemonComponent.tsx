import React from 'react';
import { POKEMON_MODEL } from '../constants';
import { PokemonDisplayComponent } from '../PokemonDisplayComponent/PokemonDisplayComponent';

interface DisplaySearchedPokemonComponentProps {
    currentPokemon: POKEMON_MODEL;
    otherPokemon: POKEMON_MODEL[];
}

export const DisplaySearchedPokemonComponent: React.FunctionComponent<DisplaySearchedPokemonComponentProps> =
    ({ currentPokemon, otherPokemon }) => {
        return (
            <>
                {otherPokemon.map((pokemon) => {
                    if (pokemon.id !== currentPokemon.id) {
                        return <PokemonDisplayComponent key={pokemon.order} pokemon={pokemon} />;
                    }
                })}
            </>
        );
    };
