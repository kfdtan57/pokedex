export const POKEMON_API_URL = 'https://pokeapi.co/api/v2/pokemon/';

export const EMPTY_POKEMON_STATS: POKEMON_MODEL = {
    id: 0,
    name: '',
    order: 0,
    weight: 0,
    sprites: '',
    height: 0,
    types: []
};

export interface POKEMON_MODEL {
    id: number;
    name: string;
    order: number;
    weight: number;
    sprites: string;
    height: number;
    types: [];
}


export interface PokemonTypes {
    slot: number;
    type: { name: string; url: string };
}